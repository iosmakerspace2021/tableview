//
//  ViewController.swift
//  TableView
//
//  Created by Olzhas Akhmetov on 17.03.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelSurname: UILabel!
    
    var name = ""
    var surname = ""
    var image = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        labelName.text = name
        labelSurname.text = surname
        ImageView.image = UIImage(named: image)
        
        
    }


}

